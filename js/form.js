const nombre = document.getElementById("elnombre");
const apellido = document.getElementById("elapellido");
const correo = document.getElementById("elcorreo");
const celular = document.getElementById("elcelular");
const contraseña = document.getElementById("elcontra");
const contraseña2 = document.getElementById("Relcontra");
const terminoycondiciones = document.getElementById("Terminoycondiciones");
const form = document.getElementById("form");
const listInputs = document.querySelectorAll("#caracteristicas"); 
   
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let condicion = validacionForm();
    if (condicion) {
      enviarFormulario();
    }
  });
  
  function validacionForm() {
    form.lastElementChild.innerHTML = "";
    let condicion = true;
    listInputs.forEach((element) => {
      element.lastElementChild.innerHTML = "";
    });
  
    if (nombre.value.length < 1 || nombre.value.trim() == "") {
      mostrarMensajeError("elnombre", "Nombre no valido*");
      condicion = false;
    }
    if (apellido.value.length < 1 || apellido.value.trim() == "") {
      mostrarMensajeError("elapellido", "Apellido no valido");
      condicion = false;
    }
    if (correo.value.length < 1 || correo.value.trim() == "") {
      mostrarMensajeError("elcorreo", "Correo no valido*");
      condicion = false;
    }
    if (celular.value.length != 9 ||celular.value.trim() == "" ||isNaN(celular.value)) 
    {
      mostrarMensajeError("elcelular", "Celular no valido*");
      condicion = false;
    }
    if (contraseña.value.length < 1 || contraseña.value.trim() == "") {
      mostrarMensajeError("elcontra", "Contraseña no valido*");
      condicion = false;
    }
    if (contraseña2.value != contraseña.value) {
      mostrarMensajeError("Relcontra", "Contraseña error*");
      condicion = false;
    }
    if (!terminoycondiciones.checked) {
      mostrarMensajeError("Terminoycondiciones", "Acepte*");
      condicion = false;
    } else {
      mostrarMensajeError("Terminoycondiciones", "");
    }
    return condicion;
  }
  
  function mostrarMensajeError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
  }
  
  function enviarFormulario() {
    form.reset();
    form.lastElementChild.innerHTML = "Listo !!";
  }

